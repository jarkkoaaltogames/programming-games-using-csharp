﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnakeDoom
{
    public partial class Form1 : Form
    { 
    Random rand;
        enum GameBoardFields
    {
        Free,
        Snake1,
        Snake2,
        Bonus
    };

    enum Directions
    {
        Up,
        Down,
        Left,
        Right
    };

    struct Snake1Coordinates
    {
        public int x;
        public int y;
    }
    struct Snake2Coordinates
    {
      public int q;
      public int w;
    }


    GameBoardFields[,] gameBoardField; //array of game board fields
    Snake1Coordinates[] snake1XY; //array of snake coordinates for body and head (head is always index [0]
    Snake2Coordinates[] snake2XY;
    int snake1Length; //after the snake eats bonus, another piece of body is added and length is increased by 1
    int snake2Length;
    Directions direction1, direction2; //direction where the head is facing
    Graphics g;
    Graphics g1;
    Graphics g2;

    public Form1()
    {
        InitializeComponent();
        gameBoardField = new GameBoardFields[22, 22]; //playable game board size is 10x10 (index 0 to 9). The other 2 indexes are for the wall
        snake1XY = new Snake1Coordinates[200]; //index 0 to 99 (10x10 is playable game board size)
        snake2XY = new Snake2Coordinates[200];
        rand = new Random();
    }

    private void frmSnake_Load(object sender, EventArgs e)
    {
        picGameBoard.Image = new Bitmap(840, 840); //35 pixels * 12 fields (10 fields of game board + 2 fields for the wall)
        g = Graphics.FromImage(picGameBoard.Image);
        g.Clear(Color.White);

        for (int i = 1; i <= 20; i++)
        {
            //top and bottom walls
            g.DrawImage(imgList.Images[6], i * 35, 0);//moving 35 pixels to the right
            g.DrawImage(imgList.Images[6], i * 35, 760); //moving 35 pixels to the right, but on the bottom of the game board (starting in index 1; so it's 35*11 = 385)
        }

        for (int i = 0; i <= 22; i++)
        {
            //left and right walls
            g.DrawImage(imgList.Images[6], 0, i * 35); //moving 35 pixels down
            g.DrawImage(imgList.Images[6], 760, i * 35); //moving 35 pixels down, but starting on the right side of the game board (starting from index 1; so it's 35 * 11 = 385
        }

        //initial snake body and head
        snake1XY[0].x = 5; //head
        snake1XY[0].y = 5;
        snake1XY[1].x = 5;//first body part
        snake1XY[1].y = 6;
        snake1XY[2].x = 5;//second body part
        snake1XY[2].y = 7;

        snake2XY[0].q = 7; //head
        snake2XY[0].w = 7;
        snake2XY[1].q = 7;//first body part
        snake2XY[1].w = 8;
        snake2XY[2].q = 7;//second body part
        snake2XY[2].w = 8;

        g.DrawImage(imgList.Images[5], 5 * 35, 5 * 35); //head
        g.DrawImage(imgList.Images[4], 5 * 35, 6 * 35); //first body part
        g.DrawImage(imgList.Images[4], 5 * 35, 7 * 35); //second body part

        gameBoardField[5, 5] = GameBoardFields.Snake1; //head
        gameBoardField[5, 6] = GameBoardFields.Snake1; //first body part
        gameBoardField[5, 7] = GameBoardFields.Snake1; //second body part

        g.DrawImage(imgList.Images[7], 7 * 35, 7 * 35); //head
        g.DrawImage(imgList.Images[8], 7 * 35, 8 * 35); //first body part
        g.DrawImage(imgList.Images[8], 7 * 35, 9 * 35); //second body part

        gameBoardField[7, 7] = GameBoardFields.Snake2; //head
        gameBoardField[7, 8] = GameBoardFields.Snake2; //first body part
        gameBoardField[7, 9] = GameBoardFields.Snake2; //second body part


        direction1 = Directions.Up;
        direction2 = Directions.Up;
            snake1Length = 3;
            snake2Length = 3;

            for (int i = 0; i < 4; i++)
        {
            Bonus();
        }
    }

    private void Bonus()
    {
        int x, y;
        var imgIndex = rand.Next(0, 4);

        //if bonus is randomly generated on a snake field or another bonus field, then keep looping and generating new coordinates
        do
        {
            x = rand.Next(1, 10);
            y = rand.Next(1, 10);
        }
        while (gameBoardField[x, y] != GameBoardFields.Free);

        gameBoardField[x, y] = GameBoardFields.Bonus; //set the field to bonus
        g.DrawImage(imgList.Images[imgIndex], x * 35, y * 35); //draw the randomly generated bonus picture in the randomly generated coordinates
    }

    private void frmSnake_KeyDown(object sender, KeyEventArgs e)
    {
        switch (e.KeyCode)
        {
            case Keys.Up:
                direction1 = Directions.Up;
                break;
            case Keys.Down:
                direction1 = Directions.Down;
                break;
            case Keys.Left:
                direction1 = Directions.Left;
                break;
            case Keys.Right:
                direction1 = Directions.Right;
                break;
            case Keys.NumPad8:
                direction2 = Directions.Up;
                break;
            case Keys.NumPad2:
                direction2 = Directions.Down;
                break;
            case Keys.NumPad4:
                direction2 = Directions.Left;
                break;
            case Keys.NumPad6:
                direction2 = Directions.Right;
                break;
            }
    }

    private void GameOver()
    {
        timer.Enabled = false;
        MessageBox.Show("GAME OVER");
    }

    private void Timer_Tick(object sender, EventArgs e)
    {
        //delete the end of the snake
        g.FillRectangle(Brushes.White, snake1XY[snake1Length - 1].x * 35,
            snake1XY[snake1Length - 1].y * 35, 35, 35);
        g.FillRectangle(Brushes.White, snake2XY[snake2Length - 1].q * 35,
           snake2XY[snake2Length - 1].w * 35, 35, 35);
            //set the game board filed of the last body part to be free
            gameBoardField[snake1XY[snake1Length - 1].x, snake1XY[snake1Length - 1].y] = GameBoardFields.Free;
            gameBoardField[snake2XY[snake2Length - 1].q, snake2XY[snake2Length - 1].w] = GameBoardFields.Free;

            //move snake field on the position of previous one
            for (int i = snake1Length; i >= 1; i--)
        {
            snake1XY[i].x = snake1XY[i - 1].x;
            snake1XY[i].y = snake1XY[i - 1].y;
        }
            for (int i = snake2Length; i >= 1; i--)
            {
                snake2XY[i].q = snake2XY[i - 1].q;
                snake2XY[i].w = snake2XY[i - 1].w;
            }

            g.DrawImage(imgList.Images[4], snake1XY[0].x * 35, snake1XY[0].y * 35);
            g.DrawImage(imgList.Images[4], snake2XY[0].q * 35, snake2XY[0].w * 35);

            //change direction of the head
            switch (direction1)
        {
            case Directions.Up:
                snake1XY[0].y = snake1XY[0].y - 1;
                break;
            case Directions.Down:
                snake1XY[0].y = snake1XY[0].y + 1;
                break;
            case Directions.Left:
                snake1XY[0].x = snake1XY[0].x - 1;
                break;
            case Directions.Right:
                snake1XY[0].x = snake1XY[0].x + 1;
                break;
        }
            switch (direction2)
            {
                case Directions.Up:
                    snake2XY[0].w = snake2XY[0].w - 1;
                    break;
                case Directions.Down:
                    snake2XY[0].w = snake2XY[0].w + 1;
                    break;
                case Directions.Left:
                    snake2XY[0].q = snake2XY[0].q - 1;
                    break;
                case Directions.Right:
                    snake2XY[0].q = snake2XY[0].q + 1;
                    break;
            }

            //check if snake hit the wall
            if ((snake1XY[0].x < 1 || snake1XY[0].x > 10 || snake1XY[0].y < 1 || snake1XY[0].y > 10) ||
                (snake2XY[0].q < 1 || snake2XY[0].q > 10 || snake2XY[0].w < 1 || snake2XY[0].w > 10))
        {
            GameOver();
            picGameBoard.Refresh();
            return;
        }

        //check if snake hit its body (if the head coordinates corespond with game board field coordinates of Snake, the snake hit its body)
        if ((gameBoardField[snake1XY[0].x, snake1XY[0].y] == GameBoardFields.Snake1) ||
                (gameBoardField[snake2XY[0].q, snake2XY[0].w] == GameBoardFields.Snake2))
        {
            GameOver();
            picGameBoard.Refresh();
            return;
        }

        //check if snake ate the bonus
        if ((gameBoardField[snake1XY[0].x, snake1XY[0].y] == GameBoardFields.Bonus)||
                (gameBoardField[snake2XY[0].q, snake2XY[0].w] == GameBoardFields.Bonus))
        {
            //add another body part after the last body part of the snake
            g.DrawImage(imgList.Images[4], snake1XY[snake1Length].x * 35,
                snake1XY[snake1Length].y * 35); 
            g.DrawImage(imgList.Images[4], snake2XY[snake2Length].q * 35,
                 snake2XY[snake2Length].w * 35);

                //set the field of the newly added body part to be of Snake
                gameBoardField[snake1XY[snake1Length].x, snake1XY[snake1Length].y] = GameBoardFields.Snake1;
                snake1Length++;
                gameBoardField[snake2XY[snake2Length].q, snake2XY[snake2Length].w] = GameBoardFields.Snake2;
                snake2Length++;

                //we only want to generate bonus if there is still a room for it on the board.
                //we don't want to draw bonus on top of a snake. If we didn't do this if statement and are game board would be filled with
                //snake body, the Bonus() method would go into infinite loop because in that method, we loop until we find a valid Free field
                if ((snake1Length < 96) || (snake2Length < 96))
                Bonus();

            this.Text = "Snake - score: " + snake1Length;
            this.Text = "Snake - score: " + snake2Length;
            }

        //draw the head
        g.DrawImage(imgList.Images[5], snake1XY[0].x * 35, snake1XY[0].y * 35);
        //change the game board field of new head to be of Snake
        gameBoardField[snake1XY[0].x, snake1XY[0].y] = GameBoardFields.Snake1;

       //draw the head
        g.DrawImage(imgList.Images[5], snake2XY[0].q * 35, snake2XY[0].w * 35);
            //change the game board field of new head to be of Snake
        gameBoardField[snake2XY[0].q, snake2XY[0].w] = GameBoardFields.Snake2;

            //refresh the whole game board
            picGameBoard.Refresh();
    }

   } 
}
