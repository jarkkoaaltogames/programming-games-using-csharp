# Programming Games Using CSharp

The goal of this course is three fold

First, is to show you how things work. Second, help you understand how things work under the hood. 
And third, to drill the basics so that they become a second nature to you.

### One Player SnakeGame

![Snake Logo](/img/snakeGame.png)

